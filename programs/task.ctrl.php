<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2017 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

$App = taskmanager_App();

$App->includeController();



/**
 * This controller manages actions that can be performed on tasks.
 *
 * @method Func_App_Taskmanager App()
 */
class taskmanager_CtrlTask extends taskmanager_Controller
{

    /**
     * {@inheritDoc}
     * @see app_CtrlRecord::toolbar()
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $Ui = $App->Ui();

        $toolbar = parent::toolbar($tableView); //$Ui->toolbar();

        $toolbar->addItem(
            $W->ActionButton(
                '',
                $this->proxy()->edit()
            )->addClass('icon',  Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG),
            0
        );

        return $toolbar;
    }




    /**
     * Actions frame for task display
     * @param taskmanager_Task $task
     * @return Widget_VBoxLayout
     */
    protected function getActionsFrame(taskmanager_Task $task)
    {
        $App = $this->App();
        $Access = $App->Access();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $actionsFrame = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'px')
            ->addClass(Func_Icons::ICON_LEFT_16)
            ->addClass('crm-actions');

        $Ctrl = $App->Controller()->Task();


        if ($Access->updateTask($task->id)) {
            $actionsFrame->addItem($W->Link($W->Icon($App->translate('Edit this task'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Ctrl->edit($task->id)));
        }

        $actionsFrame->addItem($W->Link($W->Icon($App->translate('Duplicate this task'), Func_Icons::ACTIONS_EDIT_COPY), $Ctrl->duplicate($task->id)));

        if ($Access->deleteTask($task->id)) {
            $actionsFrame->addItem(
                $W->Link($W->Icon($App->translate('Delete this task'), Func_Icons::ACTIONS_EDIT_DELETE), $Ctrl->confirmDelete($task->id))->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        return $actionsFrame;
    }



    /**
     *
     * @param array $tasks  array(taskId => '0' or '1')
     */
    public function markDone($tasks = null)
    {
        $App = $this->App();

        $recordSet = $this->getRecordSet();
        if (isset($tasks) && !is_array($tasks)) {
            $tasks = array($tasks => true);
        }

        foreach ($tasks as $taskId => $checked) {
            $record = $recordSet->get($taskId);

            if (!$record->isUpdatable()) {
                continue;
            }

            if ($record->isPlanned()) {
                continue;
            }

            if ($checked) {
                $record->setCompletion(100);
                $this->addMessage(sprintf($App->translate('The task "<b>%s</b>" has been marked as done'), $record->summary));
            } else {
                $record->setCompletion(0);
            }
        }

        return true;
    }



    /**
     *
     * @param string $start			ISO formatted datetime
     * @param string $end			ISO formatted datetime
     */
    public function ical($start = null, $end = null)
    {
        $App = $this->App();
        $Access = $App->Access();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';


        if (!isset($start)) {
            $start = bab_DateTime::now();
            $start->add(-1, BAB_DATETIME_MONTH);
            $start = $start->getIsoDateTime();
            $end = bab_DateTime::now();
            $end->add(6, BAB_DATETIME_MONTH);
            $end = $end->getIsoDateTime();
        }

        $caldav = bab_functionality::get('CalendarBackend/Caldav');

        $caldav->includeCalendarPeriod();

        $taskSet = $App->TaskSet();


        $tasks = $taskSet->select(
            $taskSet->scheduledStart->lessThanOrEqual($end)
            ->_AND_($taskSet->scheduledFinish->greaterThanOrEqual($start))
            );

        $icalEvents = array();
        foreach ($tasks as $task) {
            $start = BAB_DateTime::fromIsoDateTime($task->scheduledStart);
            $end = BAB_DateTime::fromIsoDateTime($task->scheduledFinish);

            $calendarPeriod = new bab_CalendarPeriod();
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('SUMMARY', $task->summary);
            //	        $calendarPeriod->setProperty('DESCRIPTION', $reservation->longDescription);

            $icalEvents[] = caldav_CalendarPeriod::toIcal($calendarPeriod);
        }

        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: inline; filename=calendar.ics');

        echo 'BEGIN:VCALENDAR' . "\r\n"
            . 'VERSION:2.0' . "\r\n"
            . $caldav->getProdId() . "\r\n"
            . $caldav->getTimeZone()
            . implode("\r\n", $icalEvents) . "\r\n"
            . 'END:VCALENDAR';

        die;
    }
}
