<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');



$GLOBALS['Func_PortletBackend_Taskmanager_Categories'] = array(
    'portal_tools' => taskmanager_translate('Portal tools')
);


/**
 * Crm Portlet backend
 */
class Func_PortletBackend_Taskmanager extends Func_PortletBackend
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return taskmanager_translate('Taskmanager');
    }


    /**
     * @param string $category
     * @return taskmanager_PortletDefinition_Taskmanager[]
     */
    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('taskmanager');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }

        global $Func_PortletBackend_Taskmanager_Categories;
        if (empty($category) || in_array($category, $Func_PortletBackend_Taskmanager_Categories)) {
            return array(
                'Taskmanager' => $this->portlet_Taskmanager(),
            );
        }

        return array();
    }


    /**
     * @return taskmanager_PortletDefinition_Taskmanager
     */
    public function portlet_Taskmanager()
    {
        return new taskmanager_PortletDefinition_Taskmanager();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_Taskmanager_Categories;

        return $Func_PortletBackend_Taskmanager_Categories;
    }

    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class taskmanager_PortletDefinition_Taskmanager implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    /**
     *
     */
    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('taskmanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getId()
     */
    public function getId()
    {
        return 'Taskmanager';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getName()
     */
    public function getName()
    {
        return taskmanager_translate('Taskmanager');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getDescription()
     */
    public function getDescription()
    {
        return taskmanager_translate('Configurable task view.');
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getPortlet()
     */
    public function getPortlet()
    {
        return new taskmanager_Portlet_Taskmanager();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        return array(
            array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => taskmanager_translate('Display type'),
                'options' => array(
                    array(
                        'label' => taskmanager_translate('List'),
                        'value' => 'list'
                    ),
                )
            ),
        );
    }


    /**
     *
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getRichIcon()
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'taskmanager128.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getIcon()
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'taskmanager48.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getThumbnail()
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletDefinitionInterface::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}



class taskmanager_Portlet_Taskmanager extends Widget_Item implements portlet_PortletInterface
{

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return get_class($this);
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new taskmanager_PortletDefinition_Taskmanager();
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
        bab_Registry::override('/taskmanager/' . $this->id . '/displayType', $configuration['displayType']);
    }

    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = taskmanager_App();
        $addon = bab_getAddonInfosInstance('taskmanager');

        $ctrl = $App->Controller()->Task(false);



        $box = $ctrl->filteredView(null, null, $this->id);
//        $box->setId('taskmanager_' . $this->id); // The widget item id.

        $display = $box->display($canvas);

        return $display;
    }



    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreference()
     */
    public function setPreference($name, $value)
    {

    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPortletId()
     */
    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
