<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = taskmanager_App();

$App->includeTraceableRecordSet();


/**
 * A taskmanager_Task is a task to perform.
 *
 * @property ORM_StringField                $summary
 * @property ORM_TextField                  $description
 * @property ORM_DateField                  $dueDate
 * @property ORM_DecimalField               $work               The planned work time to complete the task.
 * @property ORM_DecimalField               $actualWork         The actual work time for this task.
 * @property ORM_DecimalField               $remainingWork      The planned remaining work time to complete the task.
 * @property ORM_IntField                   $completion         Percentage completion
 * @property ORM_DateTimeField              $scheduledStart     The scheduled start date of the task
 * @property ORM_DateTimeField              $scheduledFinish    The scheduled finish date of the task
 * @property ORM_BoolField                  $scheduledAllDay
 * @property ORM_DateTimeField              $startedOn
 * @property ORM_DateTimeField              $canceledOn
 * @property ORM_DateTimeField              $completedOn
 * @property ORM_UserField                  $completedBy
 * @property ORM_UserField                  $responsible
 *
 * @property ORM_IntField                   $sortkey
 *
 * @property taskmanager_TaskCategorySet    $category           The task category
 * @property taskmanager_TaskSet            $parent             The parent task
 *
 * @method taskmanager_Task get()
 * @method taskmanager_Task newRecord()
 * @method taskmanager_Task[] select()
 */
class taskmanager_TaskSet extends app_TraceableRecordSet
{
    /**
     * @param Func_App $App
     */
    public function __construct(Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();

        $this->setDescription($App->translatable('Task'));

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('idDgOwner')
                ->setDescription($App->translatable('Delegation')),
            ORM_EnumField('type', taskmanager_translateArray(taskmanager_Task::$types))
                ->setDescription($App->translatable('Type')),
            ORM_StringField('summary')
                ->setDescription($App->translatable('Summary')),
            ORM_TextField('description')
                ->setDescription($App->translatable('Description')),
            ORM_DateField('dueDate')
                ->setDescription($App->translatable('Due date')),
            ORM_IntField('completion')
                ->setDescription($App->translatable('Completion percentage')),
            ORM_DateTimeField('scheduledStart')
                ->setDescription($App->translatable('Scheduled start date')),
            ORM_DateTimeField('scheduledFinish')
                ->setDescription($App->translatable('Scheduled finish date')),
            ORM_BoolField('scheduledAllDay')
                ->setDescription($App->translatable('Scheduled all day')),
            ORM_DateTimeField('startedOn')
                ->setDescription($App->translatable('Start date and time')),
            ORM_DateTimeField('completedOn')
                ->setDescription($App->translatable('Completion date and time')),
            ORM_DateTimeField('canceledOn')
                ->setDescription($App->translatable('Cancellation date and time')),
            ORM_UserField('responsible')
                ->setDescription($App->translatable('Responsible user')),
            ORM_UserField('completedBy')
                ->setDescription($App->translatable('User who completed the task')),
            ORM_IntField('sortkey')
                ->setDescription('Position of task in the list of siblings tasks under the summary task designated by the parent field')
        );

        $this->addFields(
            ORM_CriterionOperation('hasDescription', $this->description->startsWith('A'))
                ->setOutputOptions('Non', 'Oui!')
                ->setDescription('Description starts with A')
        );

        $this->hasOne('category', $App->TaskCategorySetClassName());

        $this->hasOne('parent', $App->TaskSetClassName());
    }


    /**
     * {@inheritDoc}
     * @see taskmanager_TraceableRecordSet::save()
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        $computedCompletion = $record->getCompletion();

        if (100 <= $computedCompletion && !$this->completedOn->isValueSet($record->completedOn)) {
            $record->completedOn = date('Y-m-d H:i:s');
        }

        $record->completion = $computedCompletion;

        return parent::save($record, $noTrace);
    }


    /**
     * @return ORM_Criterion
     */
    public function isNotCompleted()
    {
        return $this->completion->lessThan(100);
    }

    /**
     * @return ORM_Criterion
     */
    public function isCompleted()
    {
        if (!isset($this->isCompleted)) {
            $App = $this->App();
            $isCompleted = $this->completion->greaterThanOrEqual(100);
            $this->addFields(
                ORM_CriterionOperation('isCompleted', $isCompleted)
                    ->setOutputOptions($App->translate('No'), $App->translate('Yes'))
                    ->setDescription($App->translatable('Is completed'))
            );
        }

        return $this->isCompleted;
    }


    /**
     * Returns an iterator of tasks linked to the specified source,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo($source, $linkType = 'requiresTask')
    {
        return parent::selectLinkedTo($source, $linkType);
    }


    /**
     * {@inheritDoc}
     * @see app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }


    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        $Access = $this->App()->Access();
//        return $Access->canPerformActionOnSet($this, 'task:read');
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        $Access = $this->App()->Access();
        return $this->all();
//        return $Access->canPerformActionOnSet($this, 'task:update');
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        $Access = $this->App()->Access();
//        return $Access->canPerformActionOnSet($this, 'task:delete');
        return $this->isUpdatable();
    }
}




/**
 * A taskmanager_Task is a task to perform
 *
 * @property string                     $summary
 * @property string                     $description
 * @property string                     $dueDate
 * @property float                      $work               The planned work time to complete the task.
 * @property float                      $actualWork         The actual work time for this task.
 * @property float                      $remainingWork      The planned remaining work time to complete the task.
 * @property int                        $completion         Percentage completion
 * @property string                     $scheduledStart     The scheduled start date of the task
 * @property string                     $scheduledFinish    The scheduled finish date of the task
 * @property boolean                    $scheduledAllDay
 * @property string                     $startedOn
 * @property string                     $cancelledOn
 * @property string                     $completedOn
 * @property int                        $completedBy
 * @property int                        $responsible        Responsible user id
 *
 * @property int                        $sortkey
 *
 * @property taskmanager_TaskCategory   $category           The task category
 * @property taskmanager_Task           $parent             The parent task
 */
class taskmanager_Task extends app_TraceableRecord
{
    const TYPE_TODO = 1;

    public static $types =	array(
        taskmanager_Task::TYPE_TODO => 'Todo'
    );


    /**
     * {@inheritDoc}
     * @see ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->summary;
    }

    /**
     * Test if a user can work on the task
     * @return bool
     */
    public function canUserWork($user = null)
    {
        if (!bab_isUserLogged()) {
            return false;
        }

        if (!isset($user)) {
            $user = bab_getUserId();
        }

        return ($this->responsible == $user);
    }


    /**
     * @return bool
     */
    public function isDuplicable()
    {
        return true;
    }


    /**
     * Get a widget with fullpath name, can be used in a link
     * @return Widget_Displayable_Interface
     */
    public function getNameWidget()
    {
        $W = bab_Widgets();
        $name = $W->Items();
        $separator = '';

        foreach($this->getPathName() as $summary) {
            $shortened = bab_abbr($summary, BAB_ABBR_FULL_WORDS, 30);

            $label = $W->Label($separator.$shortened);
            if ($shortened !== $summary) {
                $label->setTitle($summary);
            }

            $name->addItem($label);
            $separator = ' > ';
        }

        return $name;
    }


    /**
     * @return string[]
     */
    public function getPathName()
    {
        $path = array();


        $ancestors = $this->getAncestors();
        foreach($ancestors as $task) {
            array_push($path, $task->summary);
        }

        array_push($path, $this->summary);

        return $path;
    }


    /**
     *
     * @return array
     */
    public function getAncestors()
    {
        $p = $this->parent();

        if (!$p) {
            return array();
        }

        $list = $p->getAncestors();
        $list[] = $p;

        return $list;
    }


//     /**
//      * Check if all work is done one the task
//      * @return bool
//      */
//     public function isCompleted()
//     {
//         $work = (int) round(100 * $this->work);
//         $remainingWork = (int) round(100 * $this->remainingWork);

//         $simpleCompletedTodo = (100 === (int) $this->completion);
//         $completedWorkload = 0 === $remainingWork && $work > 0;

//         return ($simpleCompletedTodo || $completedWorkload);
//     }


    /**
     * Check if some work has been done on the task
     * @return bool
     */
    public function hasStarted()
    {
        $work = (int) round(100 * $this->work);
        $actualWork = (int) round(100 * $this->actualWork);
        $remainingWork = (int) round(100 * $this->remainingWork);

        if ($work || $actualWork) {
            return ($remainingWork < $work);
        }

        return (0 !== (int) $this->completion);
    }



    /**
     *
     * @param int	$completion [0-100]
     * @return taskmanager_Task
     */
    public function setCompletion($completion)
    {
        $this->completion = $completion;

        $this->save();
        return $this;
    }



    /**
     * Get computed completion percentage or completion if the task is a simple TODO
     * @return int
     */
    public function getCompletion()
    {
        if (!$this->isPlanned()) {
            // No work duration
            return (int) $this->completion;
        }

        if (round($this->remainingWork * 100) == 0)  {
            return 100;
        }

        $total = $this->actualWork + $this->remainingWork;

        if (round($total * 100) <= 0) {
            return 0;
        }

        return (int) round(($this->actualWork * 100) / $total);
    }



    /**
     * Get completion average on sub-Tasks or completion if there are no sub-tasks
     * @return int
     */
    public function getTreeCompletion()
    {
        return $this->getTreeAvg('getCompletion');
    }


    /**
     * Test if there is planned quantity
     * @return bool
     */
    public function isPlanned()
    {
        return (0 !== (int) round(100 * $this->getWork()));
    }


    /**
     * The planned work time to complete the task.
     * @return float
     */
    public function getWork()
    {
        return (float) $this->work;
    }


    /**
     * Get work sum on sub-Tasks or work if there are no sub-tasks
     * @return int
     */
    public function getTreeWork()
    {
        return $this->getTreeSum('getWork');
    }


    /**
     * The actual work time for this task.
     * @return float
     */
    public function getActualWork()
    {
        return (float) $this->actualWork;
    }

    /**
     * Get actual work sum on sub-Tasks or actual work if there are no sub-tasks
     * @return int
     */
    public function getTreeActualWork()
    {
        return $this->getTreeSum('getActualWork');
    }


    /**
     * The planned remaining work time to complete the task.
     * @return float
     */
    public function getRemainingWork()
    {
        return (float) $this->remainingWork;
    }

    /**
     * Get remaining work sum on sub-Tasks or remaining work if there are no sub-tasks
     * @return int
     */
    public function getTreeRemainingWork()
    {
        return $this->getTreeSum('getRemainingWork');
    }


    /**
     * Returns the numbers of days left before the due date of the task.
     *
     * @param BAB_DateTime	$date		The date or null for now.
     * @return int
     */
    public function getRemainingDays($date = null)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
        if (!isset($date)) {
            $date = BAB_DateTime::now();
        }
        $nbDays = BAB_DateTime::dateDiffIso($this->dueDate, $date->getIsoDate());
        return $nbDays;
    }


    /**
     *
     */
    public function linkTo(taskmanager_Record $source, $linkType = 'requiresTask')
    {
        parent::linkTo($source, $linkType);
    }


    /**
     * @return taskmanager_Contact[]
     */
    public function getLinkedContacts($linkType = 'requiresTask')
    {
        $App = $this->App();

        $App->includeContactSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $App->ContactClassName(), $linkType);

        $contacts = array();
        foreach ($links as $link) {
            $contacts[] = $link->sourceId;
        }

        return $contacts;
    }

    /**
     * @return taskmanager_Organization[]
     */
    public function getLinkedOrganizations($linkType = 'requiresTask')
    {
        $App = $this->App();
        $App->includeOrganizationSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $App->OrganizationClassName(), $linkType);

        $orgs = array();
        foreach ($links as $link) {
            if ($link->sourceId) {
                $orgs[] = $link->sourceId;
            }
        }

        return $orgs;
    }

    /**
     * @return taskmanager_Deal[]
     */
    public function getLinkedDeals($linkType = 'requiresTask')
    {
        $App = $this->App();
        $App->includeDealSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $App->DealClassName(), $linkType);

        $deals = array();
        foreach ($links as $link) {
            $deals[] = $link->sourceId;
        }

        return $deals;
    }


    /**
     * @return taskmanager_Team[]
     */
    public function getLinkedTeams($linkType = 'requiresTask')
    {
        $App = $this->App();
        $App->includeTeamSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $App->TeamClassName(), $linkType);

        $teams = array();
        foreach ($links as $link) {
            $teams[]   = $link->sourceId;
        }

        return $teams;
    }



    /**
     * @return taskmanager_Article[]
     */
    public function getLinkedArticles($linkType = 'requiresTask')
    {
        $App = $this->App();
        $App->includeArticleSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $App->ArticleClassName(), $linkType);

        $orderItems = array();
        foreach ($links as $link) {
            $orderItems[]   = $link->sourceId;
        }

        return $orderItems;
    }




    /**
     * Get linked records
     * @return taskmanager_Record[]
     */
    public function getLinkedObjects($linkType = 'requiresTask')
    {
        $App = $this->App();
        $linkSet = $App->LinkSet();


        $criteria = $linkSet->targetId->is($this->id)
            ->_AND_($linkSet->targetClass->is(get_class($this)));

        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }


        $links = $linkSet->select($criteria);

        $records = array();
        foreach ($links as $link) {
            /*@var $link taskmanager_Link */
            $records[] = $link->getSource();
        }

        return $records;
    }


    /**
     * get linked records of the same set
     * @param taskmanager_RecordSet $recordSet
     * @return taskmanager_Record[]
     */
    public function getLinkedRecords(taskmanager_RecordSet $recordSet, $linkType = 'requiresTask')
    {
        $App = $this->App();
        $App->includeTeamSet();
        $linkSet = $App->LinkSet();

        $links = $linkSet->selectForTarget($this, $recordSet->getRecordClassName(), $linkType);

        $records = array();
        foreach ($links as $link) {
            $records[] = $link->sourceId;
        }

        return $records;
    }


    public function saveUnique()
    {
        $App = $this->App();
        $taskSet = $App->TaskSet();
        $task = $taskSet->get(
            $taskSet->startedAction->is($this->startedAction)->_AND_(
                $taskSet->responsible->is($this->responsible)
            )->_AND_(
                $taskSet->isNotCompleted()
            )
        );
        if (!isset($task)) {
            $this->save();
            return true;
        }
        return false;
    }



    /**
     * Select associated tasks.
     *
     * @return ORM_Iterator
     */
    public function selectChildTasks()
    {
        $taskSet = $this->getParentSet();
        $res = $taskSet->select($taskSet->parent->is($this->id));
        $res->orderAsc($taskSet->sortkey);
        return $res;
    }



    /**
     * @return bool
     */
    public function canDeleteChildTasks()
    {
        $access = $this->App()->Access();
        $res = $this->selectChildTasks();

        foreach ($res as $subTask) {
            if (!$access->deleteTask($subTask->id)) {
                return false;
            }
        }

        return true;
    }


    /**
     * Get a sum from a method o all sub-Tasks
     * @param string $method
     * @return Number
     */
    protected function getTreeSum($method)
    {
        $res = $this->selectChildTasks();
        if (0 === $res->count()) {
            return $this->$method();
        }

        $sum = 0;
        foreach($res as $subTask) {
            $sum += $subTask->getTreeSum($method);
        }

        return $sum;
    }


    /**
     * Get a average from a method o all sub-Tasks
     * @param string $method
     * @return Number
     */
    protected function getTreeAvg($method)
    {
        $res = $this->selectChildTasks();
        if (0 === $res->count()) {
            return $this->$method();
        }

        $sum = 0;
        foreach($res as $subTask) {
            $sum += $subTask->getTreeSum($method);
        }

        return $sum/$res->count();
    }


    /**
     * Creates a vevent icalendar string based on the task.
     *
     * @return string
     */
    public function getVevent()
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';

        $caldav = bab_functionality::get('CalendarBackend/Caldav');
        $caldav->includeCalendarPeriod();

        $scheduled = false;
        $due = false;
        if ($this->scheduledStart !== '0000-00-00 00:00:00') {
            $scheduled = true;
            $start = BAB_DateTime::fromIsoDateTime($this->scheduledStart);
            $end = BAB_DateTime::fromIsoDateTime($this->scheduledFinish);
        } elseif ($this->dueDate !== '0000-00-00') {
            $due = true;
            $start = BAB_DateTime::fromIsoDateTime($this->dueDate . ' 00:00:00');
            $end = BAB_DateTime::fromIsoDateTime($this->dueDate . '  00:00:00');
            $end->add(1, BAB_DATETIME_DAY);
        }

        $lastModified = BAB_DateTime::fromIsoDateTime($this->modifiedOn);

        $calendarPeriod = new bab_CalendarPeriod();

        if ($scheduled) {
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('DTSTART', $start->getICal(true));
            $calendarPeriod->setProperty('DTEND', $end->getICal(true));
        } elseif ($due) {
            $calendarPeriod->setProperty('DTSTART;VALUE=DATE', date('Ymd', bab_mktime($this->dueDate . ' 00:00:00')));
        }
        $calendarPeriod->setProperty('DTSTAMP', $lastModified->getICal(true));
        //$calendarPeriod->setProperty('ORGANIZER;CN='.bab_getUserName(1), 'mailto:' . bab_getUserEmail(1));
        $calendarPeriod->addAttendeeByUserId($this->responsible, 'REQ-PARTICIPANT', 'NEEDS-ACTION', 'FALSE');

        $calendarPeriod->setProperty('DESCRIPTION', $this->description);

        $calendarPeriod->setProperty('LAST-MODIFIED', $lastModified->getICal(true));

        $calendarPeriod->setProperty('SEQUENCE', '0');
        $calendarPeriod->setProperty('STATUS', 'CONFIRMED');
        $calendarPeriod->setProperty('SUMMARY', $this->summary);
        $calendarPeriod->setProperty('LOCATION', '');

        $calendarPeriod->setProperty('UID', $this->uuid);

        $vEvent = caldav_CalendarPeriod::toIcal($calendarPeriod);

        return $vEvent;
    }


    /**
     * Sends an invitation in an email notification to the task responsible.
     *
     */
    public function sendNotification()
    {
        $App = $this->App();

        $contactSet = $App->ContactSet();

        $Spooler = @bab_functionality::get('Mailspooler');

        $caldav = bab_functionality::get('CalendarBackend/Caldav');

        $caldav->includeCalendarPeriod();

        $mail = bab_mail();

        if (!$mail) {
            return;
        }

        $emailTitle = $this->summary;

        $emailMessage = '';
        $deals = $this->getLinkedDeals(null);
        foreach ($deals as $deal) {
            $emailMessage .= $App->translate('Deal') . ' : ' . $deal->getFullNumber() . "\n" . $deal->name . "\n\n";
        }

        $emailMessage .= $this->description;


        $mail->mailSubject($emailTitle);

        $htmlMessage= $mail->mailTemplate(bab_toHtml($emailMessage, BAB_HTML_BR));
        $mail->mailBody($htmlMessage);
        $mail->mailAltBody($emailMessage);

        $mail->clearAllRecipients();

        $vCalendar = 'BEGIN:VCALENDAR' . "\r\n"
            . 'METHOD:REQUEST' . "\r\n"
            . 'VERSION:2.0' . "\r\n"
            . $caldav->getProdId() . "\r\n"
            . $caldav->getTimeZone()
            . $this->getVevent() . "\r\n"
            . 'END:VCALENDAR';

        $mail->mail->Ical = $vCalendar;

        $recipient = $contactSet->get($contactSet->user->is($this->responsible));

        if (!$recipient) {
            return;
        }

        $mail->mailTo($recipient->getMainEmail());

        /* @var $Spooler Func_Mailspooler */
        if ($Spooler) {
            $Spooler->save($mail);
        }
        $mail->send();
    }
}
