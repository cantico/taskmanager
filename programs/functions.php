<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
bab_Functionality::includefile('Icons');




/**
 * Instanciates the App factory.
 *
 * @return Func_App_Taskmanager
 */
function taskmanager_App()
{
    return bab_Functionality::get('App/Taskmanager');
}


/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function taskmanager_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext')) {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('taskmanager');
        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}



/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function taskmanager_translateArray($arr)
{
	$newarr = $arr;

	foreach ($newarr as &$str) {
		$str = taskmanager_translate($str);
	}
	return $newarr;
}




/**
 * Fix datetime submited from a separated datePicker and timePicker
 * @param array $dateTime
 * @param bool $ignoreTime
 *
 * @return string
 */
function taskmanager_fixDateTime(Array $dateTime, $ignoreTime)
{
    $W = bab_Widgets();
    $date = $W->DatePicker()->getISODate($dateTime['date']);

    if (!$date || '0000-00-00' === $date) {
        return '0000-00-00 00:00:00';
    }

    $hour = $dateTime['time'];

    if (5 === mb_strlen($hour)) {
        $hour .= ':00';
    }

    if ($ignoreTime || empty($hour) || 8 !== mb_strlen($hour)) {
        $hour = '00:00:00';
    }

    return $date.' '.$hour;
}

