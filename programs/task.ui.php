<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');
$W->includePhpClass('Widget_VBoxLayout');
$W->includePhpClass('Widget_TableModelView');


/**
 * list of tasks
 *
 * @method Func_App_Taskmanager    App()
 */
class taskmanager_TaskTableView extends app_TableModelView
{
    /**
     * @param Func_App $app
     * @param string $id
     */
    public function __construct(Func_App $app = null, $id = null)
    {
        parent::__construct($app, $id);
        $this->addClass('depends-taskmanager-task');
    }


    /**
     * @param app_CtrlRecord $recordController
     * @return directorymanager_DirectoryEntryTableView
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        $this->addColumn(
            app_TableModelViewColumn($recordSet->summary)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->description)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->responsible)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->dueDate)
        );
        $this->addColumn(
            app_TableModelViewColumn($recordSet->isCompleted())
                ->addClass('widget-align-center')
        );

        $this->addColumn(
            app_TableModelViewColumn('_actions_', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-align-center')
        );
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        /* @var $record taskmanager_Task */
        if ($record->isCompleted()) {
            $this->addRowClass($row, 'task-completed');
        } else {
            if ($record->dueDate < date('Y-m-d')) {
                $this->addRowClass($row, 'task-late');
            }
        }

        return parent::handleRow($record, $row);
    }


    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = $this->App();

        /* @var $record taskmanager_Task */

        switch ($fieldPath) {
            case 'description':
                return $W->Html(nl2br($record->description));
                break;

            case 'responsible':
                return $W->Link(
                    bab_getUserName($record->responsible, true)
                );

            case 'isCompleted':
                $checkbox = $W->CheckBox();
                $checkbox->setname(array('tasks', $record->id));
                $checkbox->setAjaxAction(
                    $App->Controller()->Task()->markDone()
                );
                $checkbox->setValue($record->isCompleted());
                if (!$record->isUpdatable() || $record->isPlanned() || $record->responsible != bab_getUserId()) {
                    $checkbox->disable();
                }
                $checkbox->setTitle($App->translate('Mark as done'));
                return $checkbox;

            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Task()->edit($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                if ($record->isDeletable()) {
                    $box->addItem(
                        $W->Link(
                            '', $this->App()->Controller()->Task()->confirmDelete($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}





class taskmanager_TaskCardsView extends taskmanager_TaskTableView
{

    /**
     *
     * @param Func_App $App
     * @param string $id
     */
    public function __construct(Func_App $App = null, $id = null)
    {
        parent::__construct($App, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->TaskCardFrame($record);
        $card->addClass('crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3');
        $this->addItem($card);
        return true;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->init();

        $items = array();

        $total = $this->handleTotalDisplay();
        $selector = $this->handlePageSelector();


        if (null !== $total) {
            $items[] = $total->display($canvas);
        }

        $list = parent::getLayout();
        $items[] = $list;

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath() . 'widgets.tableview.jquery.js');
    }
}



class taskmanager_CardFrame extends app_UiObject
{
    /**
     * @param Func_Crm $crm
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_App $App, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($App);

        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * @param string $labelText
     * @param Widget_Displayable_Interface $item
     * @param string $fieldName
     * @param string $description
     * @param string $suffix
     * @param string $balloon
     *
     * @return Widget_LabelledWidget
     */
    protected function LabelledWidget($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null, $balloon = null)
    {
        $W = bab_Widgets();

        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix, $balloon);
    }
}


class taskmanager_TaskFrame extends taskmanager_CardFrame
{
    /**
     * @var taskmanager_Task
     */
    protected $task;

    /**
     * @var taskmanager_TaskSet
     */
    protected $set;


    /**
     * @param Func_App $App
     * @param taskmanager_Task $task
     * @param string $id
     * @param unknown $layout
     */
    public function __construct(Func_App $App, taskmanager_Task $task, $id = null, $layout = null)
    {
        parent::__construct($App, $id, $layout);

        $this->task = $task;
        $this->set = $task->getParentSet();
    }



    protected function responsible()
    {
        if (!$this->task->responsible) {
            return null;
        }

        return null;

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Responsible'),
            $this->task,
            $this->set->responsible
        );
    }


    protected function subTasks()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $res = $this->task->selectChildTasks();

        if (0 === $res->count()) {
            return null;
        }

        $frame = $W->Frame(null,
            $W->VBoxLayout()
                ->setSpacing(.5, 'em')
        );

        foreach($res as $subTask) {
            $card = $Ui->TaskCardFrame($subTask);
            $frame->addItem($card);
        }

        return $W->VBoxItems(
            $W->Title($App->translate('Sub-tasks')),
            $frame
        )->setVerticalSpacing(.5, 'em');
    }


    protected function getName()
    {
        $W = bab_Widgets();
        return $W->Title(implode(' / ', $this->task->getPathName()), 1);
    }

    protected function createdBy()
    {
        if (!$this->set->createdBy->isValueSet($this->task->createdBy)) {
            return null;
        }

        if ($this->task->createdBy === $this->task->responsible) {
            return null;
        }

        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Created by'),
            $this->set->createdBy->output($this->task->createdBy)
        );
    }




    protected function description()
    {
        $App = $this->App();
        return $this->LabelledWidget(
            $App->translate('Description'),
            $this->task,
            $this->set->description
        );
    }


    protected function completion()
    {
        $W = bab_Widgets();

        $completion = $this->task->getTreeCompletion();

        if (!$completion) {
            return null;
        }

        return $W->Gauge()->setProgress($completion);
    }
}


class taskmanager_TaskFullFrame extends taskmanager_TaskCardFrame
{
    /**
     * @param Func_App $App
     * @param taskmanager_Task $task
     * @param unknown $id
     */
    public function __construct(Func_App $App, taskmanager_Task $task, $id = null)
    {
        $W = bab_Widgets();
        parent::__construct($App, $task, $id, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->task = $task;
        $this->set = $task->getParentSet();

        $this->addInfos();

        $this->addClass('crm-detailed-info-frame');
        $this->addClass(Func_Icons::ICON_LEFT_24);
    }


    protected function addInfos()
    {
        $App = $this->App();
        $W = bab_Widgets();
        $this->addItem($this->getName());

        if ($this->task->isCompleted()) {
            $this->addItem($W->Title($App->translate('Completed task'), 3));
        }

        $this->addItem($this->responsible());

        $this->addItem($this->createdBy());


        $this->addItem(
            $W->FlowItems(
                $this->dueDate(),
                $this->scheduledStart(),
                $this->scheduledFinish()
            )->setHorizontalSpacing(4, 'em')
        );

        $this->addItem($this->description());

        $this->addItem($this->durationQuantities());

        $this->addItem($this->completion());

        $this->addItem($this->startedAction());


        // display sub-tasks as clickable card frames
        $this->addItem($this->subTasks());
    }


    protected function dueDate()
    {
        return $this->LabelledWidget(
            taskmanager_translate('Due date'),
            $this->task,
            $this->set->dueDate
        );
    }



    protected function scheduledDate($label, $name)
    {
        $value = $this->task->$name;
        $field = $this->set->$name;

        if (!$field->isValueSet($value)) {
            return null;
        }

        $displayable = $field->output($value);

        if ($this->task->scheduledAllDay) {
            list($displayable) = explode(' ', $displayable);
        }

        return $this->LabelledWidget($label, $displayable);
    }



    protected function scheduledStart()
    {
        return $this->scheduledDate(
            taskmanager_translate('Scheduled start date'),
            'scheduledStart'
        );
    }


    protected function scheduledFinish()
    {
        return $this->scheduledDate(
            taskmanager_translate('Scheduled finish date'),
            'scheduledFinish'
        );
    }



    /**
     * @return Widget_Layout
     */
    protected function durationQuantities()
    {
        $W = bab_Widgets();

        return $W->FlowItems(
            $this->work(),
            $this->actualWork(),
            $this->remainingWork()
        )->setHorizontalSpacing(3, 'em');
    }
}




class taskmanager_TaskCardFrame extends taskmanager_TaskFrame
{

    const SHOW_ALL = 0xFFFF;


    /**
     * @var taskmanager_Task
     */
    protected $task;

    /**
     * @var int
     */
    protected $attributes;

    protected $section = null;
    protected $inlineMenu = null;
    protected $popupMenu = null;
    protected $toolbar = null;

    static $now = null;


    /**
     * @param Func_App $App
     * @param taskmanager_Task $task
     * @param int $attributes
     * @param string $id
     */
    public function __construct(Func_App $App, taskmanager_Task $task, $attributes = taskmanager_TaskCardFrame::SHOW_ALL, $id = null)
    {
        $W = bab_Widgets();

        parent::__construct($App, $task, $id, $W->Layout());

        $Access = $App->Access();

        if (!isset(self::$now)) {
            self::$now = BAB_DateTime::now();
            self::$now = self::$now->getIsoDate();
        }


        $this->task = $task;
        $this->attributes = $attributes;

        $taskSection = $W->Section(
            $task->getNameWidget(),
            $this->taskContent = $W->VBoxItems(
                $W->RichText($task->description)
            )->setVerticalSpacing(.5,'em')->addClass('crm-small'),
            6
        );
        $taskSection->setFoldable(true, true);
        if ($task->dueDate !== '0000-00-00') {
            $dateDiff = BAB_DateTime::dateDiffIso(self::$now, $task->dueDate);
            if ($dateDiff > 1)
            {
                $days = $App->translate('days');
            } else {
                $days = $App->translate('day');
            }

            if (self::$now <  $task->dueDate) {
                $taskSection->setSubHeaderText(BAB_DateTimeUtil::relativeFutureDate($task->dueDate, false, false) . ' (' . $dateDiff . ' '.$days.')');
            } else {
                $taskSection->setSubHeaderText(BAB_DateTimeUtil::relativePastDate($task->dueDate, false, false) . ' (' . $dateDiff .' '.$days.')');
            }
        }
        $taskSection->addClass('crm-task');

        $this->section = $taskSection;
        $this->inlineMenu = $this->section->addContextMenu('inline');

        $taskSection->addClass(Func_Icons::ICON_LEFT_16);

        $this->addItem(
            $taskSection
        );

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->item->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($task->modifiedOn)));


        // add owner if not me

        if ($task->responsible > 0 && bab_getUserId() != $task->responsible) {
            $taskSection->addItem($this->responsible());
        }

        $taskSection->addItem($this->remainingWork());
        $taskSection->addItem($this->completion());

        $this->addMenuItem(
            $W->Link(
                $App->translate('View details'),
                $App->Controller()->Task()->display($task->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );
        $this->addToolbarItem(
            $W->Link(
                $App->translate('View details'),
                $App->Controller()->Task()->display($task->id)
            ),
            Func_Icons::ACTIONS_VIEW_LIST_DETAILS
        );
    }



    /**
    * @param Widget_Displayable_Item $item
    *
    * @return self
    */
    public function addLinkedItem($item)
    {
        $this->inlineMenu->addItem($item);

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     * @param bool $important
     *
     * @return self
     */
    public function addMenuItem(Widget_Displayable_Interface $item, $classname = null, $important = true)
    {
        if (!isset($this->popupMenu)) {
            $this->popupMenu = $this->section->addContextMenu('popup');
            $this->popupMenu->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);
        }
        $this->popupMenu->addItem($item);

        $item->addClass('icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }

        return $this;
    }


    /**
     * @param Widget_Displayable_Interface $item
     *
     * @return taskmanager_TaskCardFrame
     */
    public function addToolbarItem(Widget_Displayable_Interface $item, $classname = null)
    {
        $W = bab_Widgets();
        if (!isset($this->toolbar)) {
            $this->toolbar = $W->FlowLayout()
                ->setSpacing(4, 'px')
                ->addClass('icon-left-16 icon-left icon-16x16 crm-small');
            $this->section->addItem($this->toolbar);
        }
        $this->toolbar->addItem($item->addClass('widget-actionbutton'));

        $item->addClass('widget-icon icon');
        if (isset($classname)) {
            $item->addClass($classname);
        }
        return $this;
    }

}





/**
 * @return app_Editor
 */
class taskmanager_TaskEditor extends app_Editor
{
    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields()
    {
        return $this;
    }

    /**
     * Add fields into form
     * @return self
     */
    public function appendFields()
    {
        $W = bab_Widgets();

        $this->addItem($this->category());
        $this->addItem($this->summary());
        $this->addItem($this->dueDate());
        $this->addItem($this->description());
        $this->addItem($this->responsible());

        if (isset($this->record)) {
            $this->addItem(
                $W->Hidden()->setName('id')->setValue($this->record->id)
            );
        }

        return $this;
    }


    protected function category()
    {
        $App = $this->App();

        $taskSet = $App->TaskSet();

        $taskCategorySet = $App->TaskCategorySet();

        $categories = $taskCategorySet->select();

        if ($categories->count() === 0) {
            return null;
        }

        return $this->labelledField(
            $App->translate('Category'),
            app_OrmWidget($taskSet->category),
            'category'
        );
    }

    protected function summary()
    {
        $W = bab_Widgets();
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Summary'),
            $W->LineEdit()
                ->setMandatory(true, $App->translate('The task summary must not be empty.')),
            'summary'
        );
    }

    protected function dueDate()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Due date'),
            app_OrmWidget($this->getRecordSet()->dueDate),
            'dueDate'
        );
    }

    protected function description()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Description'),
            app_OrmWidget($this->getRecordSet()->description),
            'description'
        );
    }

    protected function responsible()
    {
        $App = $this->App();

        return $this->labelledField(
            $App->translate('Responsible'),
            app_OrmWidget($this->getRecordSet()->responsible),
            'responsible'
        );
    }
}

