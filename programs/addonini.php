;<?php/*

[general]
name="taskmanager"
version="0.9.80"
addon_type="EXTENSION"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="Task manager"
description.fr="Interface pour gestion de tâches"
delete=1
ov_version="8.6.97"
php_version="5.4.0"
addon_access_control="1"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="taskmanager48.png"

[addons]
libapp="0.0.1"
widgets="1.1.81"
portlets="0.23.0"

[functionalities]

; */?>