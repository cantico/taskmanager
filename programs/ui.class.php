<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';

$App = taskmanager_App();

$App->includeUi();

/**
 * The taskmanager_Ui class
 */
class taskmanager_Ui extends app_Ui
{
    /**
     * @param Func_App $app
     */
    public function __construct()
    {
        $this->setApp(taskmanager_App());
    }

    /**
     * Includes Task Ui helper functions definitions.
     */
    public function includeTask()
    {
        require_once FUNC_TASKMANAGER_UI_PATH . 'task.ui.php';
    }



    /**
     * Task table view
     * @return taskmanager_TaskTableView
     */
    public function TaskTableView()
    {
        $this->includeTask();
        return new taskmanager_TaskTableView($this->App());
    }


    /**
     * Task table view
     * @return taskmanager_TaskCardsView
     */
    public function TaskCardsView()
    {
        $this->includeTask();
        return new taskmanager_TaskCardsView($this->App());
    }


    /**
     * @param taskmanager_Task $task
     * @return taskmanager_TaskCardFrame
     */
    public function TaskCardFrame(taskmanager_Task $task)
    {
        $this->includeTask();
        return new taskmanager_TaskCardFrame($this->App(), $task);
    }


    /**
     * @param taskmanager_Task $task
     * @return taskmanager_TaskFullFrame
     */
    public function TaskFullFrame(taskmanager_Task $task)
    {
        $this->includeTask();
        return new taskmanager_TaskFullFrame($this->App(), $task);
    }


    /**
     * Add note form
     * @param 	app_Record 	$record
     * @return Widget_Item
     */
    public function AddTaskEditor(app_Record $record)
    {
        $editor = $this->TaskEditor();
        $editor->associateTo($record);

        return $editor;
    }


    /**
     * Task editor
     * @return taskmanager_TaskEditor
     */
    public function TaskEditor(taskmanager_Task $task = null)
    {
        $this->includeTask();
        return new taskmanager_TaskEditor($this->App(), $task);
    }
}
